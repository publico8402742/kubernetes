if (navigator.serviceWorker) {
	navigator.serviceWorker
		.register('/sw.js')
		.then((reg) => console.log('registro de sw exitoso'))
		.catch((error) => console.error('sw registro fallido', error));
}
// // fetch('https://fakestoreapi.com/products/1')
// // 	.then((res) => res.json())
// // 	.then(console.log);
// if (window.caches) {
// 	//si no existe, lo crea
// 	caches.open('prueba-1');
// 	caches.open('prueba-2');
// 	caches.has('prueba-3').then(console.log);
// 	caches.delete('prueba-1').then(console.log);

// 	//almacenar el index en cache
// 	caches.open('cache-v1.2').then((cache) => {
// 		cache.add('/index.html');
// 		cache.addAll(['/index.html', 'favicon.ico']).then(() => {
// 			//eliminar un archivo del cache
// 			cache.delete('/favicon.ico');
// 			//Agregar algo al cache index.html
// 			cache.put('/index.html', new Response('hola mundo'));

// 			//ver que tiene el cache
// 			// cache.match('/index.html').then((res) => {
// 			// 	res.text().then(console.log);
// 			// });
// 			cache.keys().then((keys) => {
// 				console.log(keys);
// 			});
// 		});
// 	});
// }
